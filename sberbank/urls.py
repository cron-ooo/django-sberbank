from django.urls import re_path

from . import views

app_name = 'sberbank'

urlpatterns = [  # noqa: pylint=invalid-name
    re_path('payment/callback', views.callback, name='callback'),
    re_path('payment/success', views.redirect, {'kind': 'success'}, name='success'),
    re_path('payment/fail', views.redirect, {'kind': 'fail'}, name='fail'),
    re_path('payment/status/(?P<uid>[^/]+)/', views.StatusView.as_view(), name='status'),
    re_path('payment/bindings/(?P<client_id>[^/]+)/', views.BindingsView.as_view(), name='bindings'),
    re_path('payment/binding/(?P<binding_id>[^/]+)/', views.BindingView.as_view(), name='binding'),
    re_path('payment/history/(?P<client_id>[^/]+)/', views.GetHistoryView.as_view(), name='history'),
]
